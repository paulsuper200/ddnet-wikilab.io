---
title: "Switch Layer"
explain: true
weight: 40
---

#### Tile representations of the 'switch' layer in the editor.

Switchable tile layer of maps.
Contains activators and deactivors, as well as all tiles that can be turned on and off.

{{% explain file="static/explain/switch.svg" %}}
