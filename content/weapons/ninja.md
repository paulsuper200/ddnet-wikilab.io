---
title: "Ninja"
weight: 60
---

Ninja is a different to the other weapons by being more like a power-up.

Upon collection ninja, you can dash around by firing it.
You can't switch to any other weapon while you have ninja equipped. It will run out after 15 seconds or by touching shields.
Using the dash allows quick movement and flight.

| Quickinfo  |       |
| ---------- | ----- |
| Gameskin   | ![Ninja](/images/ninja.png) |
| Crosshair  | ![Ninja crosshair](/images/ninja-crosshair.png) |
| Fire delay | 800ms |

### Dashing

Once Ninja is picked up, you can dash in any direction with a relatively long cooldown of 800ms.  

Properties:

- dash distance: <span title="14#02">14,06</span> tiles (450 base units)
- dash time: 180ms (9 ticks)
- velocity of <span title="1#18">1,56</span> tiles per tick (50 base units per tick)
- during the dash all other forces like gravity and active hooks are ignored
- ninja passes through other tees, laser doors and stopper tiles.

Whenever you hit a wall during your dash, the direction of the dash will remain unchanged, resulting in you sliding along it.  
[Video of sliding along a wall with ninja]  
After the dash any previous momentum will be redirected in the same direction of the dash.  
[Video of redirecting momentum]  

The moment you no longer have ninja, your current dash will stop immediately.

### Dash through teleporter

It is possible to pass a 1-tile broad teleporter by dashing horizontally or vertically.
Tiles that can be dashed through:

- teleporter
- freeze
- tiles that give or remove jetpack/endless hook/infinite jumps and similar tiles and similar tiles

This trick is very precise and works because dashing tees move 50 units per tick, while teleporter are only 32 units wide.
Being one tile away from the wall works for dashing through.

```
 tee pos 14 (wall)  64       114                  164        214                   264        314                   364        414                   464
 |                  |         |                    |          |                     |          |                     |          |                     |
 v                  v         v                    v          v                     v          v                     v          v                     v

 o                  o         o                    o          o                     o          o                     o          o                     o
[0, 31] [32, 63] [64, 95] [96, 127] [128, 159] [160, 191] [192, 223] [224, 255] [256, 287] [288, 319] [320, 351] [352, 383] [384, 415] [416, 447] [448, 479]
tile 0  tile 1   tile 2   tile 3    tile 4     tile 5     tile 6     tile 7     tile 8     tile 9     tile 10    tile 11    tile 12    tile 13    tile 14
```

[Video of dash through teleporter]

### Advanced Properties
- tees that you pass through during a dash will get pushed upwards and to the side
- multiple tees can get on the exact same position by dashing, to get it to work easily, use corners.

