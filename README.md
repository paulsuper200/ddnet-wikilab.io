DDNet Wiki
==========

The goal is to build a knowlegde base about Teeworlds and DDNet game mechanics.
This is intented for players learing about the physics, mappers looking for parts and developers trying to understand the physics.

Goals:

* describe game physics of DDNet's DDRace servers
* visualize as much as possible
* give exact numbers where appropriate
* give background information

Non-Goals (potential goals once we finished main goals):

* Translation: As long as english version is not considered complete, we don't plan to start translating it to other languages.
* Documentation of implementation: how the source code of Teeworlds is structured
* readable as book -> order articles in chapters
* other game modes
* difference to 0.7

## Topics:

* Weapons
  * Hammer
  * Pistol
  * Shotgun
  * Grenade Launcher
  * Laser
* Movement
  * Hook
  * Jump
* Fly Techniques
  * Hammerfly (hf)
    * normal/moving left+right
    * Kinta
    * Speedfly
    * Kinta hammering tee hooking
  * Hookfly (?hf -> kf)
    * hammerhitfly
  * Rocketfly (rf)
  * Shotgunfly
* Jetpack
* Hammer Techniques
  * Hammerhit (hh)
  * Throwing
* Gores
* Drag Parts (drag parts/top down dragging)
  * Wall hammer/Ground hammer
* Dimensions/Game world
* Useful binds
* Dummy
* Client
* Link collection to other resources

### Cloning

This repo uses [Git LFS][1] for all tutorial videos.

To clone this repository including the theme:

```bash
git clone --recursive https://gitlab.com/ddnet-wiki/ddnet-wiki.gitlab.io.git ddnet-wiki
git lfs install
```

To clone the theme if you have previously cloned DDNet-Wiki without it:

```bash
git submodule update --init --recursive
```

### Building

DDNet-Wiki is build with [hugo][2]. You can install it like this:

```bash
sudo pacman -S hugo # Arch linux
sudo apt install hugo # Debian and Ubuntu
sudo dnf install hugo # Fedora, Red Hat and CentOS
```

To get a preview of the website you can use the following command und visit ``http://localhost:1313``:

```bash
hugo server
```

## License

![CC-BY-SA](/static/images/cc-by-sa.png)

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License][3].

Work in `/static/explain/tooltipster` is licensed under MIT: https://github.com/journocode/svgtooltips

Work in `/static/explain/jquery` is licensed under MIT: https://jquery.org/license/

[1]: https://docs.gitlab.com/ee/administration/lfs/manage_large_binaries_with_git_lfs.html
[2]: https://gohugo.io/getting-started/installing/
[3]: https://creativecommons.org/licenses/by-sa/4.0/
